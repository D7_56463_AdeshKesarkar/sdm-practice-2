import "./App.css";

// import a component
import Blog from "./Blog";

// functional component
function App() {
  return (
    <div className="App">
      <h1>React Hello World</h1>

      {/* render the component */}
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
      <Blog />
    </div>
  );
}

// module.exports = App
export default App;
