import "./Blog.css";

// functional component
const Blog = () => {
  const title = "this is a my first blog";
  const description = "this is my first blogs description";
  const tags = "react,js,dom";

  return (
    <div className="blog">
      <div className="title">{title}</div>
      <div className="description">{description}</div>
      <div>{tags}</div>
    </div>
  );
};

export default Blog;
